# Blank HTML5 Starter Kit with .fitgrd

This is a small HTML5 starter kit that I made for myself. Some of the other starter templates/kits that are out there were almost too much for what I needed to get started. So I made this one.  

Like everything else, it's not fancy, and was made more for me than anything else.

For the responsive bits, I added fitgrd and the index file is set up to start implement as such.  
More info about fitgrd be found here: http://www.fitgrd.com/

All of the CSS is broken out into SCSS files and are located in the SCSS folder. Since I mostly use CodeKit to complie, I've set it up as such and all of the SCSS files will complie down into style.css in the CSS folder and is compressed.

I also threw in a blank scripts.js file in the JS folder to add in any fancy JS stuff needed.

It comes pre-packaged with this stuff:  
- .fitgrd: http://www.fitgrd.com/
- jQuery
- Modernizr